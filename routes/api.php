<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//para hacer uso del modelo
 use App\Models\Publicacion;



/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

//creacion de los metodos propios de las api, crear , eliminar, actualizar, listar


//Listar todas las Publicaciones

Route::get('publicaciones',function (){
    $publicaciones=Publicacion::get();
 return $publicaciones;
 var_dump($publicaciones);
  //  return 'Listado de publicaciones muro 2021 Wiedii';
  // validar driver MongoDB
});


//crear Publicacion

Route::post('publicaciones',function(Request $request){
    //return $request->all();
    /**
     *  
     * $request->validate([
        'id'=>'required|bigIncrements',
        'titulo'=>'required|max:50',
        'comentario'=>'required|max:100',
       // 'contenido'=>'required|max:500',
        'created_at'=>'required|timestamp'
    ]);
     */
   
    $publicacion=new Publicacion;
    $publicacion->id=$request->input('id');
    $publicacion->titulo=$request->input('titulo');
    $publicacion->comentario=$request->input('comentario');
    $publicacion->created_at=$request->input('created_at');
    $publicacion->save();
    return "Usuario Creado Con Exito";

});


//actualizar publicacion

Route::put('publicaciones/{id}',function(Request $request,$id){

    /**
     * $request->validate([
        'id'=>'required|bigIncrements',
        'titulo'=>'required|max:50',
        'comentario'=>'required|max:100',
       // 'contenido'=>'required|max:500',
        'created_at'=>'required|timestamp'
    ]);
     */
    $publicacion=Publicacion::findOrFail($id);
    //return $publicacion;
    //$publicacion->id=$request->input('id');
    $publicacion->titulo=$request->input('titulo');
    $publicacion->comentario=$request->input('comentario');
  //  $publicacion->comentario=$request->input('comentario');
    $publicacion->updated_at=$request->input('updated_at');
    $publicacion->save();
    return "Actualizacion realizada con exito   ";



});

//Eliminar Publicacion

Route::delete('publicaciones/{id}',function(Request $request,$id){
    $publicacion=Publicacion::findOrFail($id);
  //  return $publicacion;
    $publicacion->delete();
    return "Publicacion Eliminada Exitosamente";
});


//obtener una publicacion en Especifico

Route::get('publicaciones/{id}',function($id){
    $publicacion=Publicacion::findOrFail($id);
    return $publicacion;
});




