<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Rutas nuevas prueba

Route::get('prueba', function () {
    return "Hola Wiedii";
});


//para hacer uso de post implementar postman  validar token
Route::post('/prueba', function () {
    return "Hola Wiedii 2021";
});
 

//Actualizar

Route::put('/prueba/{_id}', function ($id) {
    return "Actualizando publicacion en muro con el id: "  .$id;
});